#!/usr/bin/env python3
#
# Use the XIMEA API to acquire an image from the ICCD camera
#
from ximea import xiapi
from datetime import datetime, timezone
import tifffile as tf
import argparse
import threading
import time


def store_image(img: xiapi.Image, ts: datetime, fname: str, description: str):
    with tf.TiffWriter(fname) as tw:
        data = img.get_image_data_numpy()
        tw.save(data, compress=1, photometric='minisblack',
                description=description.encode('us-ascii'),
                datetime=ts.strftime('%Y:%m:%d %H:%M:%S'),
                resolution=(1200, 1200, 'inch'))


def trigger(cam: xiapi.Camera):
    cam.set_trigger_software(1)


def main():
    parser = argparse.ArgumentParser(description="Capture an image from the ICCD camera")
    parser.add_argument("outfile", type=str,
                        help="output file name")
    parser.add_argument("--exposure", type=int,
                        metavar="USEC",
                        default=10000,
                        help="set exposure time in microseconds")
    parser.add_argument("--gain", type=float,
                        metavar="dB",
                        default=-1,
                        help="set camera gain in dB")
    parser.add_argument("--timeout", type=int,
                        metavar="MSEC",
                        default=10000,
                        help="image acquisition timeout in milliseconds")
    parser.add_argument("--description", type=str,
                        default="",
                        help="image description")
    parser.add_argument("--trigger",
                        action="store_true",
                        help="use hardware camera trigger")

    args = parser.parse_args()

    cam = xiapi.Camera()
    cam.open_device()
    cam.set_debug_level("XI_DL_FATAL")
    cam.set_imgdataformat("XI_MONO16")
    cam.set_exposure(args.exposure)
    if args.gain >= 0:
        cam.set_gain(args.gain)

    img = xiapi.Image()
    print("Starting data acquisition...")

    if args.trigger:
        cam.set_gpi_mode("XI_GPI_TRIGGER")
        cam.set_trigger_source("XI_TRG_EDGE_FALLING")
    else:
        cam.set_trigger_source("XI_TRG_SOFTWARE")
    cam.start_acquisition()

    # Schedule a software trigger
    if not args.trigger:
        t = threading.Timer(2.0, trigger, args=(cam,))
        t.start()

    t0 = time.monotonic()
    try:
        cam.get_image(img, timeout=args.timeout)
        ts = datetime.now(timezone.utc)
        print("Image read in {:.3f} seconds".format(time.monotonic()-t0))
        store_image(img, ts, args.outfile, args.description)
    except xiapi.Xi_error as err:
        if err.status == 10:
            print("Image acquisition timed out")
        else:
            raise

    print("Stopping acquisition...")
    cam.stop_acquisition()
    cam.close_device()
    print("Done")


if __name__ == "__main__":
    main()
